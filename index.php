<!doctype html>

<html>
    <head>
        <title>Exercicio</title>
        <meta charset='UTF-8'>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    </head>
    <body>
        <div class='container'>
            <div class="row justify-content-center align-items-center">
                <div style="margin-bottom:5%; margin-top:5%">
                    <h3>Sistema CRUD</h3>
                </div>
            </div>
            <div class='row'>
                <div class='col-md-8 offset-md-2 form-crud'>
                    <div class='form'>
                        <form action='' method ='post' id='form-send'>
                            <div class='form-group'>
                                <label for="tNome" id='nome'>Digite o nome completo:</label>
                                <input id='tNome' name='cName' class='form-control' required>
                            </div>
                            <div class="form-group">
                                <label for="tCpf">CPF:</label>
                                <input type="text" class="form-control" id="tCpf" name='cCpf' required>
                            </div>
                            <div class="form-group">
                                <label for="tEmail">Email</label>
                                <input type="email" class="form-control" id="tEmail" name='cEmail' required>
                            </div>

                            <div class="form-group">
                                <label for="tPhone">Telefone:</label>
                                <input class="form-control" id="tPhone" name='cPhone' required>
                            </div>
                            <div class="form-group">
                                <label for="tDataNasc">Data de Nascimento:</label>
                                <input type='text' class="form-control" id="tDataNasc" name='cDataNasc' required>
                            </div>
                            <div class="endereco">
                                <div class='form-group'>
                                    <label for="tBairro">Bairro:</label>
                                    <input type='text' class="form-control" id="tBairro" name='cBairro' required>
                                    
                                    <label for="tRua">Rua:</label>
                                    <input type='text' class="form-control" id="tRua" name='cRua' required>

                                    <label for="tNum">Numero:</label>
                                    <input type='number' class="form-control" id="tNum" name='cNum' required>

                                    <label for="tCidade">Cidade:</label>
                                    <input type='text' class="form-control" id="tCidade" name='cCidade' required>

                                    <label for="tUf">UF:</label>
                                    <input type='text' class="form-control" id="tUf" name='cUf' required>

                                    <label for="tCep">CEP:</label>
                                    <input type='text' class="form-control" id="tCep" name='cCep' required>

                                </div>
                                
                            </div>
                           
                            <div class='row' style="text-align: center;margin-top: 20px; margin-bottom:5%;">
                                <div class="col-md-6 cSend" >
                                    <button type="submit" id="send" class="btn btn-success col-md-6 cSend">Enviar</button>
                                </div>
                                <div class="col-md-6 cCancel">
                                    <button type="submit" id="tCancel" class="btn btn-danger col-md-6">Cancelar</button>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                </div>
                <div class="result class='col-md-10 offset-md-1" style="margin-top: 10%;">
                            <div class="row">
                                <div class='col-md-12'>
                                    <table class="table table-dark">
                                        <thead>
                                            <tr>
                                                <th scope="col">ID</th>
                                                <th scope="col">Nome</th>
                                                <th scope="col">Telefone</th>
                                                <th scope="col">Aniversario</th>
                                                <th scope="col">Data de Cadastro</th>
                                                <th scope="col">Endereço</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody class="tuplas">
                                            
                                        </tbody>
                                    </table>                
                                </div>
                                <div id="map" style="width: 100%; height: 400px;"></div>

                            </div>
                        </div>
            </div>
            
        </div>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCWeEpRHdnbihe7OpNMzmJzQfbT0t9i2RQ&libraries=places"></script>


        <script src="scripts/jquery.mask.js"></script>
        <script src="scripts/google_map.js"></script>
        <script src="scripts/scripts_jquery.js"></script>
         
    </body>
</html>