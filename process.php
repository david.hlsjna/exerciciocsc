<?php
    header('Content-Type: aplication/json');
    //validacao dos dados que vieram via post pelo ajax
    $nome = filter_input(INPUT_POST,'cName', FILTER_SANITIZE_SPECIAL_CHARS);
    $cpf = filter_input(INPUT_POST,'cCpf', FILTER_SANITIZE_SPECIAL_CHARS);
    $telefone = filter_input(INPUT_POST,'cPhone');
    $email = filter_input(INPUT_POST,'cEmail', FILTER_VALIDATE_EMAIL);
    $dataNascimento = filter_input(INPUT_POST,'cDataNasc');
    //pegar data atual do sistema no formato BR
    $dataAtual= date('Y/m/d');
    $dataAtualizacao=date('Y/m/d');
    //Dados do endereco
    $bairro = filter_input(INPUT_POST,'cBairro', FILTER_SANITIZE_SPECIAL_CHARS);
    $rua= filter_input(INPUT_POST,'cRua', FILTER_SANITIZE_SPECIAL_CHARS);
    $numero = filter_input(INPUT_POST,'cNumero', FILTER_SANITIZE_SPECIAL_CHARS);
    $cidade = filter_input(INPUT_POST,'cCidade', FILTER_SANITIZE_SPECIAL_CHARS);
    $cep = filter_input(INPUT_POST,'cCep', FILTER_SANITIZE_SPECIAL_CHARS);
    $uf = filter_input(INPUT_POST,'cUf', FILTER_SANITIZE_SPECIAL_CHARS);
    //Parte responsável pelo banco de dados

    $host ='127.0.0.1';
    $user = 'root';
    $password = '';
    $dbName ='exerciciocsc';

    try {
        $pdo = new PDO('mysql:host=localhost; dbname=exerciciocsc;',$user,$password);
        // set the PDO error mode to exception
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        //Sql responsavel por buscar as ocorrencias e fazer JOIN das duas tabelas
        $select = "SELECT usuario.id_user,usuario.nome,usuario.telefone,usuario.dataNascimento,usuario.dataCriacao,usuario.dataUpdate,endereco.bairro,endereco.rua,endereco.numero,endereco.cidade,endereco.uf,endereco.cep FROM usuario JOIN endereco ON usuario.id_user = endereco.id_user ORDER BY usuario.id_user DESC"; 
        //Sql para inserir dados do usuario e recuperar a ultima id inserida
        
        $sqlInsert = "INSERT into usuario (nome,cpf,email,telefone,dataNascimento,dataCriacao,dataUpdate) values ('$nome','$cpf','$email','$telefone','$dataNascimento','$dataAtual','$dataAtualizacao')";
        $insertUser = $pdo->prepare($sqlInsert);
        $insertUser->execute();
        $idUser = $pdo->lastInsertId();
        
        //Parte respnsavel por inserir o endereco com o id do ultimo user inserido
        $sqlInsertEnd ="INSERT INTO endereco(bairro, rua, numero, cidade, uf, cep, id_user) VALUES ('$bairro','$rua','$numero','$cidade','$uf','$cep','$idUser')";
        $insertEnd = $pdo->prepare($sqlInsertEnd);
        $insertEnd->execute();
        
        //Etapa necessaria para efetivar a busca no db
        $stmt = $pdo->prepare($select);
        $stmt->execute();
    
        if($stmt->rowCount() >=1 ){
            echo json_encode($stmt->fetchAll(PDO::FETCH_ASSOC));
        }else{
            echo json_encode("Deu erro"); 
        }
        
      } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
      }
      //Finaliza a conexao
      $pdo =null;