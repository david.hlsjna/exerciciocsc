-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 23-Nov-2020 às 14:15
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `exerciciocsc`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `endereco`
--

CREATE TABLE `endereco` (
  `id_end` int(11) NOT NULL,
  `bairro` varchar(60) NOT NULL,
  `rua` varchar(60) NOT NULL,
  `numero` int(11) NOT NULL,
  `cidade` varchar(60) NOT NULL,
  `uf` varchar(20) NOT NULL,
  `cep` varchar(60) NOT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `endereco`
--

INSERT INTO `endereco` (`id_end`, `bairro`, `rua`, `numero`, `cidade`, `uf`, `cep`, `id_user`) VALUES
(1, 'Jardim das Americas', 'Nossa Senhora de Lurdes', 370, 'Curitiba', 'PR', '81530-020', 1),
(2, 'Jardim das Americas', 'Nossa Senhora de Lurdes', 370, 'Curitiba', 'PR', '81530-020', 2),
(3, 'Jardim das Americas', 'Nossa Senhora de Lurdes', 370, 'Curitiba', 'PR', '81530-020', 3),
(4, 'Jardim das Americas', 'Nossa Senhora de Lurdes', 370, 'Curitiba', 'PR', '81530-020', 4),
(5, 'Boa Esperança', '38', 370, 'Cuiabá', 'MT', '78068-545', 1),
(6, 'Jardim Italia', 'Rua San Remo', 213, 'Cuiabá', 'MT', '78060-720', 5),
(7, 'Jardim Petropolis', 'Av. Fernando Correia', 2848, 'Cuiabá', 'MT', '78070-000', 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id_user` int(11) NOT NULL,
  `nome` varchar(60) NOT NULL,
  `cpf` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `telefone` varchar(60) NOT NULL,
  `dataNascimento` date NOT NULL,
  `dataCriacao` date NOT NULL,
  `dataUpdate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id_user`, `nome`, `cpf`, `email`, `telefone`, `dataNascimento`, `dataCriacao`, `dataUpdate`) VALUES
(1, 'David Henrique L. Sampaio', '555.555.555-55', 'david@gmail.com', '(66) 9 8170-7800', '1999-01-09', '2020-11-19', '2020-11-19'),
(2, 'Carla S. Montes', '027.311.456-21', 'carla@hotmail.com', '(66) 9 9940-1743', '2002-08-27', '2020-11-19', '2020-11-19'),
(3, 'Alyssa S. Montes', '027.136.651-22', 'alyssa@gmail.com', '(66) 9 9678-0456', '2018-10-31', '2020-11-19', '2020-11-19'),
(4, 'Arthur S. Montes', '033.178.381-22', 'arthur@gmail.com', '(44) 9 8131-4321', '2016-01-09', '2020-11-19', '2020-11-19'),
(5, 'Leonardo Silva', '027.319.312-45', 'leo@gmail.com', '(66) 9 9949-1743', '2000-05-03', '2020-11-20', '2020-11-20'),
(6, 'Gabriela Rodrigues', '031.456.651-12', 'gabi@email.com', '(55) 5 5555-5555', '2005-06-18', '2020-11-20', '2020-11-20');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `endereco`
--
ALTER TABLE `endereco`
  ADD PRIMARY KEY (`id_end`),
  ADD KEY `id_user` (`id_user`);

--
-- Índices para tabela `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `endereco`
--
ALTER TABLE `endereco`
  MODIFY `id_end` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de tabela `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `endereco`
--
ALTER TABLE `endereco`
  ADD CONSTRAINT `endereco_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `usuario` (`id_user`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
