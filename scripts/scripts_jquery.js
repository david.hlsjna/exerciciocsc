function convert_date(dateIn){
    var vetD;
    if(dateIn.includes('-')){
        vetD = dateIn.split('-');
        dateIn = vetD[2]+'/'+vetD[1]+'/'+vetD[0];
    }else if(dateIn.includes('/')){
        vetD = dateIn.split('/');
        dateIn = vetD[2]+'-'+vetD[1]+'-'+vetD[0];
    }
    return dateIn;

}

$(document).ready(function(){
    $('.result').hide();
    $('#tCancel').click(function () {
        location.reload();
    });


    $('#tPhone').mask('(00) 0 0000-0000');
    $('#tDataNasc').mask('00/00/0000');
    $('#tCpf').mask('000.000.000-00');
    
    $('#tCep').mask('00000-000');

    
    //AJAX
    $('#form-send').submit(function(e){
        e.preventDefault();
        
        //Validacao das variaveis
        
        var cName = $('#tNome').val();
        var cPhone = $('#tPhone').val();
        var cCpf = $('#tCpf').val();
        var cEmail = $('#tEmail').val();
        var cDataNasc = $('#tDataNasc').val();
        var cBairro = $('#tBairro').val();
        var cRua =  $('#tRua').val();
        var cNumero = $('#tNum').val();
        var cCidade =  $('#tCidade').val();
        var cUf = $('#tUf').val();
        var cCep = $('#tCep').val();
        var token = $("input[name*='_token']").val();
        
        cDataNasc = convert_date(cDataNasc);

        var endereco = cRua+', '+cNumero+', '+cBairro+ ' '+cCidade+' - '+cUf+' '+cCep;
            
        initMap();
        place_search(endereco, cName);

        $.ajax({
            url: "process.php",
            method: "POST",
            dataType: "JSON",
            data: {
                cName: cName,
                cPhone: cPhone,
                cCpf: cCpf,
                cEmail: cEmail,
                cDataNasc: cDataNasc,
                cBairro:cBairro,
                cRua:cRua,
                cNumero:cNumero,
                cCidade:cCidade,
                cUf:cUf,
                cCep:cCep,
                _token: token,
            },
            cache: false,
            success: function (e){ 
                
                $('.result').show();

                $('#tNome').val('');
                $('#tPhone').val('');
                $('#tCpf').val('');
                $('#tEmail').val('');
                $('#tDataNasc').val('');
                $('#tBairro').val('');
                $('#tRua').val('');
                $('#tNumero').val('');
                $('#tCidade').val('');
                $('#tUf').val('');
                $('#tCep').val('');

                if(e.length>0){
                    for(var i=0; i<e.length; i++){
                        
                        var end = e[i].rua +', '+ e[i].numero+', '+e[i].bairro+ ', '+e[i].cidade+' - '+e[i].uf+', '+e[i].cep;
                        e[i].dataNascimento = convert_date(e[i].dataNascimento);
                        e[i].dataCriacao = convert_date(e[i].dataCriacao);
                        var html = '<tr><th scope="col">'+e[i].id_user+'</th><td scope="col" class="name_table">'+e[i].nome+'</td><td scope="col">'+e[i].telefone+'</td><td scope="col">'+e[i].dataNascimento+'</td><td scope="col">'+e[i].dataCriacao+'</td><td scope="col" class="address">'+end+'</td></tr>';
                        $('.tuplas').prepend(html);
                        
                    }
                }else{
                    $('.tuplas').prepend(e);
                    console.log('Não possui dados');
                }
                
                //Seta o mapa para o ultimo usuario cadastrado
                $('.address').click(function(){
                    var enderecoTupla = $(this).text();
                    var nomeTupla = $(this).siblings('.name_table').text();
                    
                    place_search(enderecoTupla,nomeTupla);
                });
                
            },
            error: function (e) {
                console.log(e);
            },
        });
    });
});
