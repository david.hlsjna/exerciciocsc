let map;
let marker;

function initMap(){
    map = new google.maps.Map(
        document.getElementById('map'),
        {
            center: { lat: -15.618620, lng: -56.069628 },
            zoom: 15, 
        }
    );
    
}
function place_search(address, title){
    var request = {
        query: address,
        fields: ['name', 'geometry'],
      };
    
    var service = new google.maps.places.PlacesService(map);
    
    service.findPlaceFromQuery(request, function(results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
            marker = new google.maps.Marker(
                {
                    position: results[0].geometry.location,
                    map,
                    title: title,
                }
            );
            map.setCenter(results[0].geometry.location);
        }
    });
}
